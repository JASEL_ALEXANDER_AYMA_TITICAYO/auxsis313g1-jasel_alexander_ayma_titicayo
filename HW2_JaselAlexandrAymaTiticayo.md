
# FLEX BOX ADVENTURE
- LEVEL 1 MEDIUM= "justify-content: center;", se usa para que Arthur se mueva al centro y llegue a la manzana para que recupere su vida.
- LEVEL 2 MEDIUM= "justify-content: flex-end;", se usa para que el personaje se vaya al otro extremo para que consiga la zanahoria.
- LEVEL 3 MEDIUM= "justify-content: space-between;", para que los personajes se vayan a los extremos, generando un espacio al centro, y de tal manera llegar a sus objetivos.   
- LEVEL 4 MEDIUM= "justify-content: space-around;", genera un espacio entre el principio y el final de los personajes, y como tal llegar a sus objetivos.
- LEVEL 5 MEDIUM= "justify-content: space-evenly;" se usa para que genere un espacio igualado entre los bordes, los personajes, y el centro. 
- LEVEL 6 MEDIUM= "align-items: center;", ayuda a que los personajes se muevan al centro pero en columnas. 
- LEVEL 7 MEDIUM= "align-items: flex-end;", para que los personajes se muevan hacia el extre inferior, en forma de columnas.
- LEVEL 8 MEDIUM= "align-items: center;", se usa para centrar los personajes en forma de columna, "justify-content: center;", para que de la misma manera se centren y llegando a sus objetivos.
- LEVEL 9 MEDIUM= "align-items: flex-end;", para que los personajes se dirijan a la parte inferior del mapa, "justify-content: space-between;", para que los personajes se separen dejando espacios y llegando a sus objetivos. 
- LEVEL 10 MEDIUM= "flex-direction: column;", para que los personajes se pongan en forma de columna.
- LEVEL 11 MEDIUM= "flex-direction: row-reverse;", se usa para que los personajes se dirijan a su lado contrario acomodandose de forma inversa. 
- LEVEL 12 MEDIUM= "flex-direction: column;", para que los personajes se pongan en posicion de columna, "justify-content: flex-end;", se usa para que los personajes lleguen al final del mapa de la aprte inferior.
- LEVEL 13 MEDIUM= "flex-direction: row-reverse;", para que los personajes se dirijan al lado contrario de forma inversa, "justify-content: center;", se usa para que los personajes se vayan a centrar, "align-items: center;", para que los personajes se centren y de tal manera llegar a sus objetivos.
- LEVEL 14 MEDIUM= "order: 2;", es usado para que el 1er personaje cambie de posicion con el 2do personaje, tambien podemos usar numeros negativos, para que el personaje pueda moverse a la inversa.
- LEVEL 15 MEDIUM= "align-self: center;", para que el personaje del centro se baje al centro del mapa y como tal llegar a su objetivo.
- LEVEL 16 MEDIUM= "order: 1;", el personaje se ordena de tal manera indicada en el codigo, "align-self: flex-end;", para que el personaje se dirija a la parte inferior del mapa. 
- LEVEL 17 MEDIUM= "flex-direction: column-reverse;", para que los personajes se formen en posicion de columna y de forma inversa, "justify-content: flex-end;", para que los personajes se dirijan a la parte superior siguiendo la instruccion del codigo, "align-items: center;", se usa para que los personajes se vayan al centro del mapa y como tal llegando a sus objetivos.
- LEVEL 18 MEDIUM= "order: 2;", para que cambien de posicion los personajes segun lo necesitado, "align-self: center;", el personaje que ya cambio de posicion con el anterior codigo, se posiciona en la parte del centro y llegando a su objetivo.
- LEVEL 19 MEDIUM= "flex-wrap: wrap;", para que las monedas recorran de posicion a lado derecho, y como llega al minimo del mapa entonces las mondeas bajan.
- LEVEL 20 MEDIUM= "align-content: center;", para que las monedas se dirijan al centro.
- LEVEL 21 MEDIUM= "align-content: flex-end", se usa para que las monedas se dirijan hacia la parte inferior del mapa, luego "justify-content: center;" para que las monedas faltantes se alineen al centro y asi completando el nivel.

![Captura de pantalla 2024-03-19 110432](https://github.com/cualquiercos48lk/imagenesxde/assets/164036071/1ff86a58-6050-4c5e-bedb-62d4f7632556)
- LEVEL 22 MEDIUM= "flex-direction: column-reverse;", se uso para que los personajes se pongan en direccion de columna pero de forma inversa, "justify-content: space-around;", para que se alineen dejando un espacio entre el centro, inicio y final. y asi cada personaje este alineado a los enemigos.

![Captura de pantalla 2024-03-19 111531](https://github.com/cualquiercos48lk/imagenesxde/assets/164036071/66b592c8-4d07-48e0-9fa8-dc2067bb3461)

- LEVEL 23 MEDIUM= "flex-direction: column-reverse;", los personajes se ordenan por columna de forma de inversa, "flex-wrap: wrap-reverse;", los personajes toman el espacio correspondiente a la forma del codigo, "align-content: center;" centra a todos de manera vertical, "justify-content: center", centrea a todos, y de tal manera llegando a los objetivos. 

![Captura de pantalla 2024-03-19 112432](https://github.com/cualquiercos48lk/imagenesxde/assets/164036071/cde428cd-459e-477f-b9d9-99d832c32962)
 
- LEVEL 24 MEDIUM= "order: 3", sirve para ordenar los personajes, en este caso el de la espada larga, se va a la parte superior del enemigo, "align-self: center;", se uso para que el personaje ordenado por "order: 3", baje directa,ente al enemigo. 

![Captura de pantalla 2024-03-19 113309](https://github.com/cualquiercos48lk/imagenesxde/assets/164036071/d6b4fca1-d150-4f1d-9194-97adf7052006)

- LEVEL 1 HARD= "justify-content: center;", se usa para que Arthur se mueva al centro y llegue a la manzana para que recupere su vida.
- LEVEL 2 HARD= "justify-content: flex-end;", se usa para que el personaje se vaya al otro extremo para que consiga la zanahoria.
- LEVEL 3 HARD= "justify-content: space-between;", para que los personajes se vayan a los extremos, generando un espacio al centro, y de tal manera llegar a sus objetivos.   
- LEVEL 4 HARD= "justify-content: space-around;", genera un espacio entre el principio y el final de los personajes, y como tal llegar a sus objetivos.
- LEVEL 5 HARD= "justify-content: space-evenly;" se usa para que genere un espacio igualado entre los bordes, los personajes, y el centro. 
- LEVEL 6 HARD= "align-items: center;", ayuda a que los personajes se muevan al centro pero en columnas. 
- LEVEL 7 HARD= "align-items: flex-end;", para que los personajes se muevan hacia el extre inferior, en forma de columnas.
- LEVEL 8 HARD= "align-items: center;", se usa para centrar los personajes en forma de columna, "justify-content: center;", para que de la misma manera se centren y llegando a sus objetivos.
- LEVEL 9 HARD= "align-items: flex-end;", para que los personajes se dirijan a la parte inferior del mapa, "justify-content: space-between;", para que los personajes se separen dejando espacios y llegando a sus objetivos. 
- LEVEL 10 HARD= "flex-direction: column;", para que los personajes se pongan en forma de columna.
- LEVEL 11 HARD= "flex-direction: row-reverse;", se usa para que los personajes se dirijan a su lado contrario acomodandose de forma inversa. 
- LEVEL 12 HARD= "flex-direction: column;", para que los personajes se pongan en posicion de columna, "justify-content: flex-end;", se usa para que los personajes lleguen al final del mapa de la aprte inferior.
- LEVEL 13 HARD= "flex-direction: row-reverse;", para que los personajes se dirijan al lado contrario de forma inversa, "justify-content: center;", se usa para que los personajes se vayan a centrar, "align-items: center;", para que los personajes se centren y de tal manera llegar a sus objetivos.
- LEVEL 14 HARD= "order: 2;", es usado para que el 1er personaje cambie de posicion con el 2do personaje, tambien podemos usar numeros negativos, para que el personaje pueda moverse a la inversa.
- LEVEL 15 HARD= "align-self: center;", para que el personaje del centro se baje al centro del mapa y como tal llegar a su objetivo.
- LEVEL 16 HARD= "order: 1;", el personaje se ordena de tal manera indicada en el codigo, "align-self: flex-end;", para que el personaje se dirija a la parte inferior del mapa. 
- LEVEL 17 HARD= "flex-direction: column-reverse;", para que los personajes se formen en posicion de columna y de forma inversa, "justify-content: flex-end;", para que los personajes se dirijan a la parte superior siguiendo la instruccion del codigo, "align-items: center;", se usa para que los personajes se vayan al centro del mapa y como tal llegando a sus objetivos.
- LEVEL 18 HARD= "order: 2;", para que cambien de posicion los personajes segun lo necesitado, "align-self: center;", el personaje que ya cambio de posicion con el anterior codigo, se posiciona en la parte del centro y llegando a su objetivo.
- LEVEL 19 HARD= "flex-wrap: wrap;", para que las monedas recorran de posicion a lado derecho, y como llega al minimo del mapa entonces las mondeas bajan.
- LEVEL 20 HARD= "align-content: center;", para que las monedas se dirijan al centro.
- LEVEL 21 HARD= "align-content: flex-end", se usa para que las monedas se dirijan hacia la parte inferior del mapa, luego "justify-content: center;" para que las monedas faltantes se alineen al centro y asi completando el nivel."

![Captura de pantalla 2024-03-19 114645](https://github.com/cualquiercos48lk/imagenesxde/assets/164036071/4451143a-37dd-4e1b-bf24-e570087a7af0)

- LEVEL 22 HARD= "flex-direction: column-reverse", se uso para que los personajes se pongan en direccion de columna pero de forma inversa, "justify-content: space-around;", para que se alineen dejando un espacio entre el centro, inicio y final. y asi cada personaje este alineado a los enemigos.  
 
![Captura de pantalla 2024-03-19 115044](https://github.com/cualquiercos48lk/imagenesxde/assets/164036071/7bd2b0c8-5a26-4817-8680-a0157a81bdca)

- LEVEL 23 HARD= "flex-direction: column-reverse;", los personajes se ordenan por columna de forma de inversa, "flex-wrap: wrap-reverse;", los personajes toman el espacio correspondiente a la forma del codigo, "align-content: center;" centra a todos de manera vertical, "justify-content: center", centrea a todos, y de tal manera llegando a los objetivos. 

![Captura de pantalla 2024-03-19 115304](https://github.com/cualquiercos48lk/imagenesxde/assets/164036071/a5389592-d04a-4406-a104-adbbbb9254aa)

- LEVEL 24 HARD= "order: 3", sirve para ordenar los personajes, en este caso el de la espada larga, se va a la parte superior del enemigo, "align-self: center;", se uso para que el personaje ordenado por "order: 3", baje directa,ente al enemigo. 

![Captura de pantalla 2024-03-19 115443](https://github.com/cualquiercos48lk/imagenesxde/assets/164036071/10ec0120-eaa7-4e21-997d-1fc8cee13d69)


